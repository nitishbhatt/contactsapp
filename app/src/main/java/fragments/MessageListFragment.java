package fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import adapter.SQLiteListAdapter;
import nitish.com.contactsapp.R;
import repository.SQLiteHelper;
import repository.Utils;

/**
 * Created by Nitish on 26-Nov-16.
 */
public class MessageListFragment extends Fragment {

    private SQLiteHelper mSQLiteHelper;
    private SQLiteDatabase mSQLiteDB;
    private Cursor mcursor;
    private SQLiteListAdapter mListAdapter;

    private ArrayList<String> ID_ArrayList = new ArrayList<String>();
    private ArrayList<String> OTP_ArrayList = new ArrayList<String>();
    private ArrayList<String> NAME_ArrayList = new ArrayList<String>();
    private ArrayList<String> PHONENUMBER_ArrayList = new ArrayList<String>();
    private ArrayList<String> TIME_ArrayList = new ArrayList<String>();
    private ListView mMessageListView;
    private View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        mView = inflater.inflate(R.layout.fragment_message_list, container, false);
        mMessageListView = (ListView) mView.findViewById(R.id.listView_messagelist);

        mSQLiteHelper = new SQLiteHelper(getContext());
        return mView;
    }

    @Override
    public void onResume() {

        ShowSQLiteDBdata();

        super.onResume();
    }

    private void ShowSQLiteDBdata() {

        mSQLiteDB = mSQLiteHelper.getWritableDatabase();

        mcursor = mSQLiteDB.rawQuery("SELECT * FROM MessageTable", null);

        OTP_ArrayList.clear();
        NAME_ArrayList.clear();
        PHONENUMBER_ArrayList.clear();
        TIME_ArrayList.clear();

        if (mcursor.moveToFirst()) {
            do {
                ID_ArrayList.add(mcursor.getString(mcursor.getColumnIndex(Utils.KEY_ID)));

                NAME_ArrayList.add(mcursor.getString(mcursor.getColumnIndex(Utils.KEY_NAME)));

                PHONENUMBER_ArrayList.add(mcursor.getString(mcursor.getColumnIndex(Utils.KEY_PHONENUMBER)));

                TIME_ArrayList.add(mcursor.getString(mcursor.getColumnIndex(Utils.KEY_TIME)));

                OTP_ArrayList.add(mcursor.getString(mcursor.getColumnIndex(Utils.KEY_OTP)));


            } while (mcursor.moveToNext());
        }

        mListAdapter = new SQLiteListAdapter(getContext(),

                ID_ArrayList,
                NAME_ArrayList,
                PHONENUMBER_ArrayList,
                TIME_ArrayList,
                OTP_ArrayList

        );

        mMessageListView.setAdapter(mListAdapter);

        mcursor.close();
    }
}
