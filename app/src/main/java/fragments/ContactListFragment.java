package fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import adapter.ContactListAdapter;
import model.ContactsModel;
import model.DataModel;
import nitish.com.contactsapp.MessageActivity;
import nitish.com.contactsapp.R;
import repository.ContactRepository;

/**
 * Created by Nitish on 26-Nov-16.
 */

public class ContactListFragment extends Fragment {

    private ListView mListView;
    private View mView;
    private DataModel dataModel;
    private ContactRepository repository;
    private ContactsModel mcontactsModel;
    private ArrayList<ContactsModel> mContactName;
    private ContactListAdapter mContactListAdapter = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        mView = inflater.inflate(R.layout.fragment_contact_list, container, false);
        mListView = (ListView) mView.findViewById(R.id.listview_contact);
        repository = new ContactRepository();


        mContactName = new ArrayList<>();

        dataModel = repository.getContactDetails(getActivity());
        for (ContactsModel item : dataModel.getNameList()) {
            mContactName.add(item);
        }
        mContactListAdapter = new ContactListAdapter(getContext(), R.id.list_text, mContactName);
        mListView.setAdapter(mContactListAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                mView = view;
                mcontactsModel = (ContactsModel) view.getTag();
                Intent intent = new Intent(getContext(), MessageActivity.class);
                Bundle contactbundle = new Bundle();
                contactbundle.putParcelable("contacts", mcontactsModel);
                intent.putExtras(contactbundle);
                startActivity(intent);

            }
        });
        return mView;
    }


}
