package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Nitish on 26-Nov-16.
 */
public class ContactsModel implements Parcelable {
    public static final Parcelable.Creator<ContactsModel> CREATOR = new Creator<ContactsModel>() {
        @Override
        public ContactsModel createFromParcel(Parcel source) {
            return new ContactsModel(source);
        }

        @Override
        public ContactsModel[] newArray(int size) {
            return new ContactsModel[size];
        }
    };
    private String id;
    private String phone;
    private String email;
    private String name;
    private String gender;

    private ContactsModel(Parcel in) {
        this.name = in.readString();
        this.phone = in.readString();
        this.email = in.readString();
    }

    public ContactsModel() {
        this.name = name;
        this.id = id;
        this.phone = phone;
        this.email = email;
        this.gender = gender;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "ClassPojo [id = " + id + ", phone = " + phone + ", email = " + email + ", name = " + name + ", gender = " + gender + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.phone);
        dest.writeString(this.gender);
    }
}