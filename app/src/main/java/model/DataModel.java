package model;

import java.util.List;

/**
 * Created by Nitish on 26-Nov-16.
 */
public class DataModel {
    private List<ContactsModel> nameList;
    private List<String> emailList;
    private List<Integer> phoneList;
    private List<String> genderList;

    public List<ContactsModel> getNameList() {
        return nameList;
    }

    public void setNameList(List<ContactsModel> nameList) {
        this.nameList = nameList;
    }

    public List<String> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<String> emailList) {
        this.emailList = emailList;
    }

    public List<String> getGenderList() {
        return genderList;
    }

    public void setGenderList(List<String> genderList) {
        this.genderList = genderList;
    }

    public List<Integer> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<Integer> phoneList) {
        this.phoneList = phoneList;
    }
}
