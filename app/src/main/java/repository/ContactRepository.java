package repository;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import model.ContactsModel;
import model.DataModel;

/**
 * Created by Nitish on 26-Nov-16.
 */
public class ContactRepository {
    public DataModel getContactDetails(Context _context) {
        DataModel dataModel = new DataModel();
        List<ContactsModel> nameList = new ArrayList<>();
        StringBuffer stringBuffer = new StringBuffer();
        BufferedReader bufferread = null;
        try {
            String fileName = "contacts.json";
            bufferread = new BufferedReader(new InputStreamReader(_context.getAssets().open(fileName)));
            String temp;
            while ((temp = bufferread.readLine()) != null)
                stringBuffer.append(temp);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                bufferread.close(); // stop reading
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        String myjsonstring = stringBuffer.toString();
        // Try to parse JSON
        try {
            // Creating JSONObject from String
            JSONObject jsonObjMain = new JSONObject(myjsonstring);

            // Creating JSONArray from JSONObject
            JSONArray jsonArray = jsonObjMain.getJSONArray("ContactsModel");

            ContactsModel contactsModel;

            for (int i = 0; i < jsonArray.length(); i++) {
                // Creating JSONObject from JSONArray
                JSONObject jsonObj = jsonArray.getJSONObject(i);

                // Getting data from individual JSONObject
                contactsModel = new ContactsModel();
                contactsModel.setId(jsonObj.getString("id"));
                contactsModel.setName(jsonObj.getString("name"));
                contactsModel.setEmail(jsonObj.getString("email"));
                contactsModel.setGender(jsonObj.getString("gender"));
                contactsModel.setPhone(jsonObj.getString("phone"));

                nameList.add(contactsModel);
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }


        dataModel.setNameList(nameList);
        return dataModel;
    }
}
