package repository;

/**
 * Created by Nitish on 27-Nov-16.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper {


    public SQLiteHelper(Context context) {

        super(context, Utils.DATABASE_NAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        String CREATE_TABLE = "CREATE TABLE " + Utils.TABLE_NAME + " (" + Utils.KEY_ID + " INTEGER PRIMARY KEY, " + Utils.KEY_NAME + " VARCHAR, " + Utils.KEY_PHONENUMBER + " VARCHAR, " + Utils.KEY_TIME + " VARCHAR, " + Utils.KEY_OTP + " VARCHAR)";
        database.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Utils.TABLE_NAME);
        onCreate(db);

    }

}
