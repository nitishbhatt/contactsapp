package adapter;

/**
 * Created by Nitish on 27-Nov-16.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import nitish.com.contactsapp.R;

public class SQLiteListAdapter extends BaseAdapter {

    private Context mcontext;
    private ArrayList<String> mID;
    private ArrayList<String> muserName;
    private ArrayList<String> mPhoneNumber;
    private ArrayList<String> mTime;
    private ArrayList<String> mOTP;
    private LayoutInflater mLayoutInflater;

    public SQLiteListAdapter(
            Context context,
            ArrayList<String> id,
            ArrayList<String> name,
            ArrayList<String> phone,
            ArrayList<String> time,
            ArrayList<String> otp
    ) {

        this.mcontext = context;
        this.muserName = name;
        this.mPhoneNumber = phone;
        this.mTime = time;
        this.mOTP = otp;
        this.mID = id;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return muserName.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public View getView(int position, View child, ViewGroup parent) {

        Holder holder;


        if (child == null) {
            mLayoutInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            child = mLayoutInflater.inflate(R.layout.listview_data_layout, null);

            holder = new Holder();

            holder.textviewname = (TextView) child.findViewById(R.id.textViewNAME);
            holder.textviewphonenumber = (TextView) child.findViewById(R.id.textViewPHONE);
            holder.textviewtime = (TextView) child.findViewById(R.id.textViewTIME);
            holder.textviewotp = (TextView) child.findViewById(R.id.textViewOTP);

            child.setTag(holder);

        } else {

            holder = (Holder) child.getTag();
        }
        holder.textviewname.setText(muserName.get(position));
        holder.textviewphonenumber.setText(mPhoneNumber.get(position));
        holder.textviewtime.setText(mTime.get(position));
        holder.textviewotp.setText(mOTP.get(position));

        return child;
    }

    public class Holder {
        TextView textviewname;
        TextView textviewphonenumber;
        TextView textviewtime;
        TextView textviewotp;
    }

}