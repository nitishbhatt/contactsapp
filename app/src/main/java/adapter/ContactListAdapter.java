package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import model.ContactsModel;
import nitish.com.contactsapp.R;

/**
 * Created by Nitish on 26-Nov-16.
 */
public class ContactListAdapter extends ArrayAdapter<ContactsModel> {

    public ArrayList<ContactsModel> contactsModels;
    private TextView mTextviewContactName;
    public ContactListAdapter(Context context, int textViewResourceId, ArrayList<ContactsModel> contactsModels) {
        super(context, textViewResourceId, contactsModels);
        this.contactsModels = new ArrayList<ContactsModel>();
        this.contactsModels.addAll(contactsModels);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ContactsModel contactsModel = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }
        mTextviewContactName = (TextView) convertView.findViewById(R.id.list_text);
        mTextviewContactName.setText(contactsModel.getName());
        convertView.setTag(contactsModel);
        return convertView;
    }
}
