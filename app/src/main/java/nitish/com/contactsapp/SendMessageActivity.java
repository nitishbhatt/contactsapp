package nitish.com.contactsapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import nexmo.ResponseHandler;
import repository.Utils;

/**
 * Created by Nitish on 26-Nov-16.
 */
public class SendMessageActivity extends AppCompatActivity {


    private TextView mMessageText;
    private Button mSendMessageButton;
    private String mPhone, mName, mSQLiteQuery, mTime;
    private int mOTP;
    private SQLiteDatabase mSQLiteDB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);

        Random rnd = new Random();
        mOTP = 100000 + rnd.nextInt(900000);
        mMessageText = (TextView) findViewById(R.id.edittext_msg);
        mSendMessageButton = (Button) findViewById(R.id.btn_send);
        mSendMessageButton = (Button) findViewById(R.id.btn_send);
        mPhone = this.getIntent().getStringExtra("phone");
        mName = this.getIntent().getStringExtra("name");
        mMessageText.setText("your OTP is : " + mOTP);
        mSendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendMessage();

            }
        });


    }

    public void sendMessage() {

        mTime = DateFormat.getDateTimeInstance().format(new Date());
        DBCreate();

        SubmitData2SQLiteDB();

        String url = "https://rest.nexmo.com/sms/json";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("api_key", Utils.API_KEY);
        params.put("api_secret", Utils.API_SECRET);
        params.put("from", "NEXMO");
        params.put("to", mPhone);
        params.put("text", mMessageText.getText().toString());

        final JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, new JSONObject(params), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int total_messages = response.getInt("message-count");
                            System.out.println("total number of messages retrieved: " + total_messages);
                            JSONArray messages = response.getJSONArray("messages");
                            String messages_status[] = new String[20];
                            String messages_id[] = new String[20];
                            String messages_to[] = new String[20];
                            String messages_remaining_balance[] = new String[20];
                            String messages_price[] = new String[20];
                            String messages_network[] = new String[20];

                            for (int i = 0; i < messages.length(); i++) {
                                JSONObject current_message = messages.getJSONObject(i);
                                messages_status[i] = current_message.get("status").toString();
                                messages_id[i] = current_message.get("message-id").toString();
                                messages_to[i] = current_message.get("to").toString();
                                messages_remaining_balance[i] = current_message.get("remaining-balance").toString();
                                messages_price[i] = current_message.get("message-price").toString();
                                messages_network[i] = current_message.get("network").toString();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Response: " + response.toString());
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub

                    }
                });
        ResponseHandler.getInstance(this).addToRequestQueue(jsObjRequest);
    }


    public void DBCreate() {

        mSQLiteDB = openOrCreateDatabase(Utils.DATABASE_NAME, Context.MODE_PRIVATE, null);

        mSQLiteDB.execSQL("CREATE TABLE IF NOT EXISTS " + Utils.TABLE_NAME + "(" + Utils.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + Utils.KEY_NAME + " VARCHAR," + Utils.KEY_PHONENUMBER + " VARCHAR," + Utils.KEY_TIME + " VARCHAR," + Utils.KEY_OTP + " VARCHAR);");
    }

    public void SubmitData2SQLiteDB() {


        mSQLiteQuery = "INSERT INTO " + Utils.TABLE_NAME + "(" + Utils.KEY_NAME + "," + Utils.KEY_PHONENUMBER + "," + Utils.KEY_TIME + "," + Utils.KEY_OTP + ") VALUES('" + mName + "', '" + mPhone + "', '" + mTime + "','" + mOTP + "');";
        mSQLiteDB.execSQL(mSQLiteQuery);

    }


}
