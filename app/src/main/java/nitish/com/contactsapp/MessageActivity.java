package nitish.com.contactsapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import model.ContactsModel;


/**
 * Created by Nitish on 26-Nov-16.
 */
public class MessageActivity extends AppCompatActivity {

    private TextView mNametextview, memailtextview, mphonetextview;
    private Button mSendButton;
    private ContactsModel contactsModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        initControl();
        Bundle bundle = this.getIntent().getExtras();
        contactsModel = bundle.getParcelable("contacts");
        mNametextview.setText(contactsModel.getName());
        memailtextview.setText(contactsModel.getEmail());
        mphonetextview.setText(contactsModel.getPhone());
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), SendMessageActivity.class);
                intent.putExtra("phone",contactsModel.getPhone());
                intent.putExtra("name",contactsModel.getName());
                startActivity(intent);


            }
        });
    }

    private void initControl() {
        mNametextview = (TextView) findViewById(R.id.textView_name);
        memailtextview = (TextView) findViewById(R.id.textView_email);
        mphonetextview = (TextView) findViewById(R.id.textView_phone);
        mSendButton = (Button) findViewById(R.id.button_send);
    }

}
